# FLIR_Thermal_Camera_Calibration
# README #

**FLIR Thermal Camera Calibration** is a tool for calibrating the FLIR thermal camera. 

### How do I get set up? ###
1 Setup dependencies
```bash
sudo apt-get install python-opencv
```
```bash
sudo pip install numpy
```
2 Clone this package
```
git clone git@bitbucket.org:castacks/flir_thermal_camera_calibration.git
```

3 Configure param.json

* "IMG_DIR": source image directory.
* "IMG_TYPE": camera type, "thermal" or "visual".
* "IMG_FORMAT": input image format.
* "BOARD_TYPE": calibration board type, configure when "IMG_TYPE" is "thermal", option has "chessboard" or "wired".
* "TEMPLATE_DIR" : template directory.
* "TEMPLATE_FORMAT": input template format.
* "EXTRACT_TEMPLATE": 0: templates existed. 1: extract template before calibrating.
* "TEMPLATE_SIZE": set desired template size, i.e 26.
* "BOARD_ROW": number of rows on the calibration board.
* "BOARD_COL": number of cols on the calibration board.
* "MATCH_THRESH": Template Matching threshold, default 0.9.
* "VISUALIZE": display the found features. 0 or 1.
* "SAVE_UNDISTORT_IMG": save undistorted input images. 0 or 1.
* "SAVE_CALIB_RESULT" : save calibration result. 0 or 1.
* "RESULT_DIR": save calibration result directory.

4 Run Run_Thermal_Calibration.py   
```
python Run_Thermal_Calibration.py
```


### Who do I talk to? ###

* Ruixuan Liu (ruixuanl@andrew.cmu.edu)

